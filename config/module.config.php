<?php

namespace Bitkorn\Help;

use Bitkorn\Help\Controller\Ajax\HelpAjaxController;
use Bitkorn\Help\Controller\HelpController;
use Bitkorn\Help\Controller\Rest\HelpBookRestController;
use Bitkorn\Help\Controller\Rest\HelpChapterRestController;
use Bitkorn\Help\Controller\Rest\HelpSiteRestController;
use Bitkorn\Help\Controller\Rest\HelpTextRestController;
use Bitkorn\Help\Factory\Controller\Ajax\HelpAjaxControllerFactory;
use Bitkorn\Help\Factory\Controller\HelpControllerFactory;
use Bitkorn\Help\Factory\Controller\Rest\HelpBookRestControllerFactory;
use Bitkorn\Help\Factory\Controller\Rest\HelpChapterRestControllerFactory;
use Bitkorn\Help\Factory\Controller\Rest\HelpSiteRestControllerFactory;
use Bitkorn\Help\Factory\Controller\Rest\HelpTextRestControllerFactory;
use Bitkorn\Help\Factory\Form\HelpTextFormFactory;
use Bitkorn\Help\Factory\Service\HelpServiceFactory;
use Bitkorn\Help\Factory\Table\HelpBookTableFactory;
use Bitkorn\Help\Factory\Table\HelpChapterTableFactory;
use Bitkorn\Help\Factory\Table\HelpSiteTableFactory;
use Bitkorn\Help\Factory\Table\HelpSiteTextTableFactory;
use Bitkorn\Help\Factory\Table\HelpTextTableFactory;
use Bitkorn\Help\Factory\Table\ViewHelpTextTableFactory;
use Bitkorn\Help\Form\HelpTextForm;
use Bitkorn\Help\Service\HelpService;
use Bitkorn\Help\Table\HelpBookTable;
use Bitkorn\Help\Table\HelpChapterTable;
use Bitkorn\Help\Table\HelpSiteTable;
use Bitkorn\Help\Table\HelpSiteTextTable;
use Bitkorn\Help\Table\HelpTextTable;
use Bitkorn\Help\Table\ViewHelpTextTable;
use Bitkorn\Help\View\ChapterHeading;
use Bitkorn\Help\View\TableOfContents;
use Laminas\Router\Http\Segment;

return [
    'router'          => [
        'routes' => [
            'bitkorn_help_helpcompletehtml' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/help-book-html[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => HelpController::class,
                        'action'     => 'helpBookHtml'
                    ],
                ],
            ],
            /*
             * help - AJAX
             */
            'bitkorn_help_ajax_helpbyid'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/help-by-id[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => HelpAjaxController::class,
                        'action'     => 'getHelpById'
                    ],
                ],
            ],
            'bitkorn_help_ajax_helpbook'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/help-book[/:id[/:with_texts]]',
                    'constraints' => [
                        'id'         => '[0-9]+',
                        'with_texts' => '[0-1]?',
                    ],
                    'defaults'    => [
                        'controller' => HelpAjaxController::class,
                        'action'     => 'helpBook'
                    ],
                ],
            ],
            /*
             * help - REST
             */
            'bitkorn_help_rest_helpbook'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/help-rest-book[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => HelpBookRestController::class
                    ],
                ],
            ],
            'bitkorn_help_rest_helpchapter' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/help-rest-chapter[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => HelpChapterRestController::class
                    ],
                ],
            ],
            'bitkorn_help_rest_helpsite'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/help-rest-site[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => HelpSiteRestController::class
                    ],
                ],
            ],
            'bitkorn_help_rest_helptext'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/help-rest-text[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => HelpTextRestController::class
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            HelpController::class            => HelpControllerFactory::class,
            HelpAjaxController::class        => HelpAjaxControllerFactory::class,
            // REST
            HelpBookRestController::class    => HelpBookRestControllerFactory::class,
            HelpChapterRestController::class => HelpChapterRestControllerFactory::class,
            HelpSiteRestController::class    => HelpSiteRestControllerFactory::class,
            HelpTextRestController::class    => HelpTextRestControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [
            // service
            HelpService::class       => HelpServiceFactory::class,
            // table
            HelpBookTable::class     => HelpBookTableFactory::class,
            HelpChapterTable::class  => HelpChapterTableFactory::class,
            HelpSiteTable::class     => HelpSiteTableFactory::class,
            HelpSiteTextTable::class => HelpSiteTextTableFactory::class,
            HelpTextTable::class     => HelpTextTableFactory::class,
            ViewHelpTextTable::class => ViewHelpTextTableFactory::class,
            // form
            HelpTextForm::class      => HelpTextFormFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [
        ],
        'invokables' => [
            ChapterHeading::class  => ChapterHeading::class,
            TableOfContents::class => TableOfContents::class,
        ],
        'aliases'    => [
            'chapterHeading'  => ChapterHeading::class,
            'tableOfContents' => TableOfContents::class,
        ],
    ],
    'view_manager'    => [
        'template_map'        => [
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'bitkorn_help'    => [
    ]
];

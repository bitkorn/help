<?php

namespace Bitkorn\Help\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class HelpChapterTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'help_chapter';

    /**
     * @param int $helpChapterId
     * @return array From database view `view_help_chapter`.
     */
    public function getHelpChapter(int $helpChapterId): array
    {
        $select = new Select('view_help_chapter');
        try {
            $select->where(['help_chapter_id' => $helpChapterId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $helpBookId
     * @return array From database view `view_help_chapter`.
     */
    public function getHelpBookChapters(int $helpBookId = 0): array
    {
        $select = new Select('view_help_chapter');
        try {
            $select->order('help_chapter_no1 ASC, help_chapter_no2 ASC, help_chapter_no3 ASC');
            if($helpBookId) {
                $select->where(['help_book_id' => $helpBookId]);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

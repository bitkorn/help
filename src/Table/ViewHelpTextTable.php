<?php

namespace Bitkorn\Help\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ViewHelpTextTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_help_text';

    /**
     * @param int $helpBookId
     * @param bool $withTexts If TRUE then without columns *_text & *_subj
     * @return array
     */
    public function getHelpBook(int $helpBookId, bool $withTexts = true): array
    {
        $select = $this->sql->select();
        try {
            if (!$withTexts) {
                $select->columns([
                    'help_chapter_no',
                    'help_book_no',
                    'help_chapter_no1',
                    'help_chapter_no2',
                    'help_chapter_no3',
                    'help_text_no',
                    'help_book_id',
                    'help_book_head',
                    'help_chapter_id',
                    'help_chapter_head',
                    'help_text_id',
                    'help_text_head'
                ]);
            }
            $select->where(['help_book_id' => $helpBookId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getHelpText(int $helpTextId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['help_text_id' => $helpTextId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

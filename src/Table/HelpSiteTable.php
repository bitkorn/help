<?php

namespace Bitkorn\Help\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class HelpSiteTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'help_site';

    /**
     * @param int $helpSiteId
     * @return array
     */
    public function getHelpSite(int $helpSiteId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['help_site_id' => $helpSiteId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

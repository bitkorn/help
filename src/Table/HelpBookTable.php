<?php

namespace Bitkorn\Help\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class HelpBookTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'help_book';

    /**
     * @return array
     */
    public function getHelpBooks(): array
    {
        $select = $this->sql->select();
        try {
            $select->order('help_book_no ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

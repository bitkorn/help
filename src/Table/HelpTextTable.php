<?php

namespace Bitkorn\Help\Table;

use Bitkorn\Help\Entity\HelpTextEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class HelpTextTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'help_text';

    /**
     * @param int $helpTextId
     * @return array
     */
    public function getHelpText(int $helpTextId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['help_text_id' => $helpTextId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getHelpChapterTexts(int $helpChapterId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['help_chapter_id' => $helpChapterId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertHelpText(HelpTextEntity $helpTextEntity): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($helpTextEntity->getStorageInsert());
            if ($this->insertWith($insert) == 1) {
                return $this->adapter->driver->getConnection()->getLastGeneratedValue('public.help_text_help_text_id_seq');
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateHelpText(HelpTextEntity $helpTextEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set($helpTextEntity->getStorageUpdate());
            $update->where(['help_text_id' => $helpTextEntity->getPrimaryKeyValue()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}

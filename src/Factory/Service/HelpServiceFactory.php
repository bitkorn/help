<?php

namespace Bitkorn\Help\Factory\Service;

use Bitkorn\Help\Service\HelpService;
use Bitkorn\Help\Table\HelpBookTable;
use Bitkorn\Help\Table\HelpChapterTable;
use Bitkorn\Help\Table\HelpSiteTable;
use Bitkorn\Help\Table\HelpSiteTextTable;
use Bitkorn\Help\Table\HelpTextTable;
use Bitkorn\Help\Table\ViewHelpTextTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class HelpServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new HelpService();
        $service->setLogger($container->get('logger'));
        $service->setHelpBookTable($container->get(HelpBookTable::class));
        $service->setHelpChapterTable($container->get(HelpChapterTable::class));
        $service->setHelpSiteTable($container->get(HelpSiteTable::class));
        $service->setHelpSiteTextTable($container->get(HelpSiteTextTable::class));
        $service->setHelpTextTable($container->get(HelpTextTable::class));
        $service->setViewHelpTextTable($container->get(ViewHelpTextTable::class));
        return $service;
    }
}

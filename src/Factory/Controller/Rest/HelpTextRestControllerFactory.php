<?php

namespace Bitkorn\Help\Factory\Controller\Rest;

use Bitkorn\Help\Controller\Rest\HelpTextRestController;
use Bitkorn\Help\Form\HelpTextForm;
use Bitkorn\Help\Service\HelpService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class HelpTextRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new HelpTextRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setHelpService($container->get(HelpService::class));
        $controller->setHelpTextForm($container->get(HelpTextForm::class));
        return $controller;
    }
}

<?php

namespace Bitkorn\Help\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Filter\HtmlPurifierFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Filter\StringTrim;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\Digits;
use Laminas\Validator\StringLength;

class HelpTextForm extends AbstractForm implements InputFilterProviderInterface, AdapterAwareInterface
{
    protected Adapter $adapter;

    /**
     * Set db adapter
     *
     * @param Adapter $adapter
     * @return AdapterAwareInterface
     */
    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'help_text_id']);
        }
        $this->add(['name' => 'help_chapter_id']);
        $this->add(['name' => 'help_text_no']);
        $this->add(['name' => 'help_text_head']);
        $this->add(['name' => 'help_text_text']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];
        if ($this->primaryKeyAvailable) {
            $filter['help_text_id'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => FilterChainStringSanitize::class]
                ], 'validators' => [
                    ['name' => Digits::class]
                ]
            ];
        }

        $filter['help_chapter_id'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                [
                    'name'    => Digits::class,
                    'options' => [
                        'break_chain_on_failure' => true
                    ]
                ],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table'   => 'help_chapter',
                        'field'   => 'help_chapter_id',
                    ]
                ]
            ]
        ];

        $filter['help_text_no'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => Digits::class]
            ]
        ];

        $filter['help_text_head'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 0,
                        'max'      => 180,
                    ]
                ]
            ]
        ];

        $filter['help_text_text'] = [
            'required'      => true,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlPurifierFilter::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 0,
                        'max'      => 60000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}

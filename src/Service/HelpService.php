<?php

namespace Bitkorn\Help\Service;

use Bitkorn\Help\Entity\HelpTextEntity;
use Bitkorn\Help\Table\HelpBookTable;
use Bitkorn\Help\Table\HelpChapterTable;
use Bitkorn\Help\Table\HelpSiteTable;
use Bitkorn\Help\Table\HelpSiteTextTable;
use Bitkorn\Help\Table\HelpTextTable;
use Bitkorn\Help\Table\ViewHelpTextTable;
use Bitkorn\Trinket\Service\AbstractService;

class HelpService extends AbstractService
{
    protected HelpBookTable $helpBookTable;
    protected HelpChapterTable $helpChapterTable;
    protected HelpSiteTable $helpSiteTable;
    protected HelpSiteTextTable $helpSiteTextTable;
    protected HelpTextTable $helpTextTable;
    protected ViewHelpTextTable $viewHelpTextTable;

    public function setHelpBookTable(HelpBookTable $helpBookTable): void
    {
        $this->helpBookTable = $helpBookTable;
    }

    public function setHelpChapterTable(HelpChapterTable $helpChapterTable): void
    {
        $this->helpChapterTable = $helpChapterTable;
    }

    public function setHelpSiteTable(HelpSiteTable $helpSiteTable): void
    {
        $this->helpSiteTable = $helpSiteTable;
    }

    public function setHelpSiteTextTable(HelpSiteTextTable $helpSiteTextTable): void
    {
        $this->helpSiteTextTable = $helpSiteTextTable;
    }

    public function setHelpTextTable(HelpTextTable $helpTextTable): void
    {
        $this->helpTextTable = $helpTextTable;
    }

    public function setViewHelpTextTable(ViewHelpTextTable $viewHelpTextTable): void
    {
        $this->viewHelpTextTable = $viewHelpTextTable;
    }

    /**
     * @return array
     */
    public function getHelpBooks(): array
    {
        return $this->helpBookTable->getHelpBooks();
    }

    /**
     * @param int $helpBookId
     * @return array
     */
    public function getHelpBookChapters(int $helpBookId): array
    {
        return $this->helpChapterTable->getHelpBookChapters($helpBookId);
    }

    /**
     * @param int $id
     * @return array From db.view_help_text
     */
    public function getHelpById(int $id): array
    {
        if (empty($helpText = $this->viewHelpTextTable->getHelpText($id)) || !is_array($helpText)) {
            return [];
        }
        return $helpText;
    }

    /**
     * @param int $helpBookId
     * @param bool $withTexts If TRUE then without columns *_text & *_subj
     * @return array From db.view_help_text
     */
    public function getHelpBook(int $helpBookId, bool $withTexts = true): array
    {
        return $this->viewHelpTextTable->getHelpBook($helpBookId, $withTexts);
    }

    /**
     * @param HelpTextEntity $helpTextEntity
     * @return int
     */
    public function insertHelpText(HelpTextEntity $helpTextEntity): int
    {
        return $this->helpTextTable->insertHelpText($helpTextEntity);
    }

    /**
     * @param HelpTextEntity $helpTextEntity
     * @return bool
     */
    public function updateHelpText(HelpTextEntity $helpTextEntity): bool
    {
        return $this->helpTextTable->updateHelpText($helpTextEntity) >= 0;
    }
}

<?php

namespace Bitkorn\Help\Controller\Rest;

use Bitkorn\Help\Entity\HelpTextEntity;
use Bitkorn\Help\Form\HelpTextForm;
use Bitkorn\Help\Service\HelpService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;

class HelpTextRestController extends AbstractUserRestController
{
    protected HelpService $helpService;
    protected HelpTextForm $helpTextForm;

    public function setHelpService(HelpService $helpService): void
    {
        $this->helpService = $helpService;
    }

    public function setHelpTextForm(HelpTextForm $helpTextForm): void
    {
        $this->helpTextForm = $helpTextForm;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->helpTextForm->init();
        $this->helpTextForm->setData($data);
        if (!$this->helpTextForm->isValid()) {
            $jsonModel->addMessages($this->helpTextForm->getMessages());
            return $jsonModel;
        }
        $helpText = new HelpTextEntity();
        if (!$helpText->exchangeArrayFromDatabase($this->helpTextForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($helpTextId = $this->helpService->insertHelpText($helpText))) {
            $jsonModel->setVal($helpTextId);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->helpTextForm->setPrimaryKeyAvailable(true);
        $data['help_text_id'] = $id;
        $this->helpTextForm->init();
        $this->helpTextForm->setData($data);
        if (!$this->helpTextForm->isValid()) {
            $jsonModel->addMessages($this->helpTextForm->getMessages());
            return $jsonModel;
        }
        $helpText = new HelpTextEntity();
        if (!$helpText->exchangeArrayFromDatabase($this->helpTextForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->helpService->updateHelpText($helpText)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}

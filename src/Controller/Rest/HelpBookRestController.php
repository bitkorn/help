<?php

namespace Bitkorn\Help\Controller\Rest;

use Bitkorn\Help\Service\HelpService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;

class HelpBookRestController extends AbstractUserRestController
{
    protected HelpService $helpService;

    public function setHelpService(HelpService $helpService): void
    {
        $this->helpService = $helpService;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->helpService->getHelpBooks());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

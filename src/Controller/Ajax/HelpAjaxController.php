<?php

namespace Bitkorn\Help\Controller\Ajax;

use Bitkorn\Help\Service\HelpService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class HelpAjaxController extends AbstractUserController
{
    protected HelpService $helpService;

    public function setHelpService(HelpService $helpService): void
    {
        $this->helpService = $helpService;
    }

    /**
     * @return JsonModel
     */
    public function getHelpByIdAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $id = filter_var($this->params('id', 0), FILTER_SANITIZE_NUMBER_INT);
        if (!empty($id) && !empty($help = $this->helpService->getHelpById($id))) {
            $jsonModel->setObj($help);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET base_url/book_id/with_texts
     * @return JsonModel
     */
    public function helpBookAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $id = filter_var($this->params('id', 0), FILTER_SANITIZE_NUMBER_INT);
        $withText = filter_var($this->params('with_texts', 0), FILTER_SANITIZE_NUMBER_INT) > 0;
        if (!empty($id) && !empty($book = $this->helpService->getHelpBook($id, $withText))) {
            $jsonModel->setArr($book);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

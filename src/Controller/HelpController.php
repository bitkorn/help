<?php

namespace Bitkorn\Help\Controller;

use Bitkorn\Help\Service\HelpService;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\View\Model\ViewModel;

class HelpController extends AbstractUserController
{
    protected HelpService $helpService;

    public function setHelpService(HelpService $helpService): void
    {
        $this->helpService = $helpService;
    }

    /**
     * @return ViewModel
     */
    public function helpBookHtmlAction(): ViewModel
    {
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $viewModel;
        }
        $id = filter_var($this->params('id', 0), FILTER_SANITIZE_NUMBER_INT);
        if (empty($texts = $this->helpService->getHelpBook($id))) {
            return $viewModel;
        }
        $viewModel->setVariable('first', $texts[0]);
        $viewModel->setVariable('texts', $texts);
        return $viewModel;
    }
}

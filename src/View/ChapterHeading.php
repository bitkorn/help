<?php

namespace Bitkorn\Help\View;

use Laminas\View\Helper\AbstractHelper;

class ChapterHeading extends AbstractHelper
{

    /**
     * @param array $helpText
     * @param string $lastChapterNo
     * @return string
     */
    public function __invoke(array $helpText, string $lastChapterNo = ''): string
    {
        if (empty($helpText) || $helpText['help_chapter_no'] == $lastChapterNo) {
            return '';
        }
        $html = '';
        if (!empty($helpText['help_chapter_no3'])) {
            if (!empty($helpText['help_chapter_head'])) {
                $html .= '<h4 id="' . $helpText['help_chapter_no'] . '">'
                    . $helpText['help_chapter_no1'] . '.' . $helpText['help_chapter_no2'] . '.' . $helpText['help_chapter_no3']
                    . ' - ' . $helpText['help_chapter_head'] . '</h4>';
            }
            if (!empty($helpText['help_chapter_subj'])) {
                $html .= '<p>' . $helpText['help_chapter_subj'] . '</p>';
            }
        } else if (!empty($helpText['help_chapter_no2'])) {
            if (!empty($helpText['help_chapter_head'])) {
                $html .= '<h3 id="' . $helpText['help_chapter_no'] . '">'
                    . $helpText['help_chapter_no1'] . '.' . $helpText['help_chapter_no2'] . ' - ' . $helpText['help_chapter_head'] . '</h3>';
            }
            if (!empty($helpText['help_chapter_subj'])) {
                $html .= '<p>' . $helpText['help_chapter_subj'] . '</p>';
            }
        } else if (!empty($helpText['help_chapter_no1'])) {
            if (!empty($helpText['help_chapter_head'])) {
                $html .= '<h2 id="' . $helpText['help_chapter_no'] . '">'
                    . $helpText['help_chapter_no1'] . ' - ' . $helpText['help_chapter_head'] . '</h2>';
            }
            if (!empty($helpText['help_chapter_subj'])) {
                $html .= '<p>' . $helpText['help_chapter_subj'] . '</p>';
            }
        }
        return $html;
    }
}

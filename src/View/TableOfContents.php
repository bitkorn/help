<?php

namespace Bitkorn\Help\View;

use Laminas\View\Helper\AbstractHelper;

class TableOfContents extends AbstractHelper
{

    const TAB = '&nbsp;&nbsp;&nbsp;&nbsp;';

    /**
     * @param array $texts
     * @return string
     */
    public function __invoke(array $texts): string
    {
        if (empty($texts) || !is_array($texts)) {
            return '';
        }
        $html = '<ul class="w3-ul">';
        $lastChapterNo = '';
        foreach ($texts as $text) {
            if ($lastChapterNo == $text['help_chapter_no']) {
                continue;
            }
            $html .= '<li><a href="#' . $text['help_chapter_no'] . '">';
            if (!empty($text['help_chapter_no3'])) {
                if (!empty($text['help_chapter_head'])) {
                    $html .= self::TAB . self::TAB
                        . $text['help_chapter_no1'] . '.' . $text['help_chapter_no2'] . '.' . $text['help_chapter_no3'] . ' - ' . $text['help_chapter_head'];
                } else {
                    $html .= self::TAB . self::TAB
                        . $text['help_chapter_no1'] . '.' . $text['help_chapter_no2'] . '.' . $text['help_chapter_no3'];
                }
            } else if (!empty($text['help_chapter_no2'])) {
                if (!empty($text['help_chapter_head'])) {
                    $html .= self::TAB . $text['help_chapter_no1'] . '.' . $text['help_chapter_no2'] . ' - ' . $text['help_chapter_head'];
                } else {
                    $html .= self::TAB . $text['help_chapter_no1'] . '.' . $text['help_chapter_no2'];
                }
            } else if (!empty($text['help_chapter_no1'])) {
                if (!empty($text['help_chapter_head'])) {
                    $html .= $text['help_chapter_no1'] . ' - ' . $text['help_chapter_head'];
                } else {
                    $html .= $text['help_chapter_no1'];
                }
            }
            $html .= '</a></li>';
            $lastChapterNo = $text['help_chapter_no'];
        }
        $html .= '</ul>';
        return $html;
    }
}

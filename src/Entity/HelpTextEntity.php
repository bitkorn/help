<?php

namespace Bitkorn\Help\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class HelpTextEntity extends AbstractEntity
{
    public array $mapping = [
        'help_text_id'    => 'help_text_id',
        'help_chapter_id' => 'help_chapter_id',
        'help_text_no'    => 'help_text_no',
        'help_text_head'  => 'help_text_head',
        'help_text_text'  => 'help_text_text',
    ];

    protected $primaryKey = 'help_text_id';
    protected array $databaseFieldsInsert = [
        'help_chapter_id',
        'help_text_no',
        'help_text_head',
        'help_text_text',
    ];
    protected array $databaseFieldsUpdate = [
        'help_text_no',
        'help_text_head',
        'help_text_text',
    ];

    public function getHelpTextId(): int
    {
        if (!isset($this->storage['help_text_id'])) {
            return 0;
        }
        return $this->storage['help_text_id'];
    }

    public function setHelpTextId(int $helpTextId): void
    {
        $this->storage['help_text_id'] = $helpTextId;
    }

    public function getHelpChapterId(): int
    {
        if (!isset($this->storage['help_chapter_id'])) {
            return 0;
        }
        return $this->storage['help_chapter_id'];
    }

    public function setHelpChapterId(int $helpChapterId): void
    {
        $this->storage['help_chapter_id'] = $helpChapterId;
    }

    public function getHelpTextNo(): int
    {
        if (!isset($this->storage['help_text_no'])) {
            return 0;
        }
        return $this->storage['help_text_no'];
    }

    public function setHelpTextNo(int $helpTextNo): void
    {
        $this->storage['help_text_no'] = $helpTextNo;
    }

    public function getHelpTextHead(): string
    {
        if (!isset($this->storage['help_text_head'])) {
            return '';
        }
        return $this->storage['help_text_head'];
    }

    public function setHelpTextHead(string $helpTextHead): void
    {
        $this->storage['help_text_head'] = $helpTextHead;
    }

    public function getHelpTextText(): string
    {
        if (!isset($this->storage['help_text_text'])) {
            return '';
        }
        return $this->storage['help_text_text'];
    }

    public function setHelpTextText(string $helpTextText): void
    {
        $this->storage['help_text_text'] = $helpTextText;
    }
}

# ZF3 Module bitkorn/help

Help Texts can be displayed individually, in context.

Help texts organized in book groups can be downloaded as a PDF book.

The texts are in HTML for structuring and styling the output.
create table help_book
(
    help_book_id   serial       not null
        constraint help_book_pk
            primary key,
    help_book_head varchar(200) not null,
    help_book_subj text
);

alter table help_book owner to postgres;

create table help_chapter
(
    help_chapter_id        serial       not null
        constraint help_chapter_pk
            primary key,
    help_book_id           integer      not null
        constraint help_chapter_help_book_help_book_id_fk
            references help_book,
    help_chapter_id_parent integer
        constraint help_chapter_help_chapter_help_chapter_id_fk
            references help_chapter,
    help_chapter_no        integer      not null,
    help_chapter_head      varchar(200) not null,
    help_chapter_subj      text
);

alter table help_chapter owner to postgres;

create
unique index help_chapter_help_book_id_help_chapter_no_uindex
	on help_chapter (help_book_id, help_chapter_no);

create table help_text
(
    help_text_id        integer      not null
        constraint help_text_pk
            primary key,
    help_chapter_id     integer      not null
        constraint help_text_help_chapter_help_chapter_id_fk
            references help_chapter,
    help_text_id_parent integer
        constraint help_text_help_text_help_text_id_fk
            references help_text,
    help_text_no        integer      not null,
    help_text_head      varchar(200) not null,
    help_text_text      text         not null
);

alter table help_text owner to postgres;

create
unique index help_text_help_chapter_id_help_text_no_uindex
	on help_text (help_chapter_id, help_text_no);


create table help
(
    help_uuid uuid not null
        constraint help_pk
            primary key,
    help_key varchar(200) not null,
    help_heading varchar(100) not null,
    help_subject text default ''::text not null,
    help_text text not null,
    help_uuid_parent uuid
        constraint help_help_help_uuid_fk
            references help,
    help_book varchar(40) default ''::character varying not null
);

comment on column help.help_key is 'key for translation (.po)';

comment on column help.help_uuid_parent is 'hierarchy in a book';

comment on column help.help_book is 'help-groups to arrange them in a book';

alter table help owner to postgres;

create unique index help_help_key_uindex
    on help (help_key);

